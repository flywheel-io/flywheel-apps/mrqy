FROM python:3.8 as base

RUN apt-get update && apt-get install -y \
        git \
        && \
    pip install \
        pydicom \
        numpy \
        medpy \
        matplotlib \
        scipy \
        umap \
        pandas \
        scikit-image \
        scikit-learn \
        && \
    rm -r ${HOME}/.cache/pip

RUN cd /usr/local/bin && git clone https://github.com/ccipd/MRQy.git && \
    chmod a+x /usr/local/bin/MRQy/src/mrqy/*py

RUN apt-get remove -y \
        curl \
        mercurial \
        zip \
        && \
    apt-get autoremove -y && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Dev install. git for pip editable install.
RUN apt-get update && apt-get install -y git && \ 
    pip install "poetry==1.1.2"

# Installing main dependencies
COPY pyproject.toml poetry.lock $FLYWHEEL/
RUN poetry install --no-dev

# Installing the current project (most likely to change, above layer can be cached)
# Note: poetry requires a README.md to install the current project
COPY run.py manifest.json README.md $FLYWHEEL/
COPY fw_gear_mrqy $FLYWHEEL/fw_gear_mrqy
RUN poetry install --no-dev

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py && \
    chmod a+x $FLYWHEEL/fw_gear_mrqy/run_mrqy.sh
ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]
