import logging
import typing

import flywheel
from flywheel_gear_toolkit import GearToolkitContext
from flywheel_gear_toolkit.utils.curator import HierarchyCurator
from flywheel_gear_toolkit.utils.walker import Walker

log = logging.getLogger(__name__)


def is_file_match(
    filename: flywheel.FileEntry,
    filetype: str = None,
    modality: str = None,
    measurement: str = None,
):
    """
    Ensure the scan is an MR image and has been classified. Discard localizers and calibration scans.
    Optionally, narrow down the results to only the measurement of interest (i.e., T1)
    """
    # Get rid of localizers
    if not filename.get("classification"):
        return False

    if any(i in ['Calibration','Localizer'] for i in filename.get("classification", {}).get('Intent',[])):
        return False

    # Filter dicoms vs. niftis
    if filetype and filename.type != filetype:
        return False

    if modality and filename.modality != "MR":
        return False

    if not filename.get("classification", {}).get("Measurement"):
        return False

    if measurement:
        if not any(m in measurement for m in filename.get("classification", {}).get("Measurement",[])):
            return False

    return True


class FileFinder(HierarchyCurator):
    """A curator to find files on the session containers"""

    def __init__(
        self,
        *args,
        filetype: str = None,
        modality: str = None,
        measurement: str = None,
        context: GearToolkitContext = None,
        **kwargs,
    ):
        super(FileFinder, self).__init__(*args, **kwargs)
        self.filetype = filetype
        self.modality = modality
        self.measurement = measurement
        self.files = []
        self.acquisitions = []
        self.context = context

    def curate_acquisition(self, acquisition: flywheel.Acquisition):
        """Override the empty method in curator.py"""
        for f in acquisition.files:
            if is_file_match(
                filename=f,
                filetype=self.filetype,
                modality=self.modality,
                measurement=self.measurement,
            ):
                self.files.append(f)

    def curate_session(self, session: flywheel.Session):
        """Override the empty method in curator.py"""
        # reset the file collection
        self.files = []
        for acq in session.acquisitions.iter_find():
            self.curate_acquisition(acq)


def get_matching_files(
    root,
    filetype=None,
    modality=None,
    measurement=None,
    context=None
):
    """Returns a list of files matching the criteria given."""
    log.info("Walk the hierarchy and find matching files...")
    my_walker = Walker(root)
    finder = FileFinder(
        filetype=filetype,
        modality=modality,
        measurement=measurement,
        context=context,
        )
    for container in my_walker.walk():
        finder.curate_container(container)


    # De-dupe
    ids = list(set([f.get('file_id') for f in finder.files]))
    final_list = []
    for f in finder.files:
        if f.get('file_id') in ids:
            final_list.append(f)
            ids.remove(f.get('file_id'))

    return final_list
