import glob
import logging
import os.path as op
import shutil
from os import mkdir

import pandas as pd

log = logging.getLogger(__name__)


def retrieve_batch_results(work_dir, output_dir):
    """
    Locate the result files and selectively copy the non-image ones to /flywheel/v0/output,
    where they can be zipped and escape deletion during container teardown.
    """
    src = op.join(work_dir, "UserInterface/Data")
    dst = op.join(output_dir, "MRQy")
    if not op.exists(dst):
        mkdir(dst)

    # Find the tsv's that have been modified by add_results_metadata
    filenames = glob.glob(op.join(src, "**", "*tsv"))
    for f in filenames:
        if "results" not in f:
            base = op.basename(op.dirname(f)).replace(" ", "_")
            try:
                shutil.copyfile(f, op.join(dst, base + ".tsv"))
                log.debug(f'Copied to {op.join(dst, base + ".tsv")}')
            except Exception as e:
                log.error("Problem copying %s", f)
                log.error(e)
    if len(glob.glob(op.join(dst, "*"))) > 0:
        shutil.make_archive(dst, "zip", dst)


def add_results_metadata(csv_loc, classification, acq_label):
    """
    Find the participant's csv. Modify the values to be those of interest and
    add classification information.
    Args:
        csv_loc (path): Directory where the acquisition's files reside, needed for the
                        analysis output csv. The csv contains the information for the
                        metadata to be cleaned and reported out.
        classification (dict): metadata information about the Intent, Features,
                                and Measurement in the acquisition
        acq_label (str): corresponds to the acquisition_label in the hierarchy
    Returns:
        dictionary of dataframe values to add to the metadata dictionary.
    """
    csv = glob.glob(op.join(glob.escape(csv_loc), "*csv"))
    if csv:
        log.debug("Modifying %s", csv)
        df = pd.DataFrame(pd.read_csv(csv[0]))
        # Refine the reported msrs; useful for discarding the initial measures that do not appear
        # to be all that informative. (voxel number and such)
        # coi = df.columns.get_loc("MEAN")
        # drop_cols = df.columns[1:coi]
        # If not dropping the first few columns, then at least drop the
        # list of output QC .pngs column
        df = df.drop(columns="Name of Images")
        df.insert(1, "acquisition", acq_label)
        classification = check_classification(classification)
        df = df.assign(**classification)
        df.rename(
            {"Intent": "intent", "Measurement": "measurement", "Features": "features"},
            inplace=True,
        )
        # Overwrite original csv, so the zipped version is edited.
        tsv = op.splitext(csv[0])[0] + ".tsv"
        df.to_csv(tsv, sep="\t")
        # Send to .metadata.json
        return df.to_dict("records")[0]


def check_classification(classification):
    """Insure that the columns will all be ordered and populated similarly between participants."""
    for c in ["Intent", "Features", "Measurement"]:
        if c not in classification.keys():
            classification[c] = "null"
    return classification
