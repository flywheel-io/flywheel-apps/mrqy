"""Main module."""

import logging
import os
import os.path as op
import typing as t
import subprocess as sp
from zipfile import ZipFile
from pathlib import Path
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_mrqy.parser import gunzip
from fw_gear_mrqy.results import retrieve_batch_results, add_results_metadata

log = logging.getLogger(__name__)


def run(gear_context, input_files: t.List, work_dir, output_dir):
    """
    Create the folders for MRQy to populate with analyses for the input files
    designated. Analysis output are refined to produce metadata in alignment with
    XNAT implementations of MRQy, for the time being.
    """
    # Make a place for MRQy to land the results, where MRQy expects
    Path.mkdir(Path(op.join(output_dir, 'UserInterface/Data')), parents=True, exist_ok=True)

    log.info(f'Processing {len(input_files)} possible files')
    metadata_dict = {}
    # Process the files
    for i,f in enumerate(input_files):
        tmp_file = op.join(work_dir, f.name)
        f.download(tmp_file)

        # Make a spot for the unzipped files to land
        input_dir = op.splitext(tmp_file)[0]
        if '.' in input_dir:
            input_dir = op.splitext(input_dir)[0]
        Path.mkdir(Path(input_dir), exist_ok=True)
        result_name = op.basename(input_dir)

        # Unzip the files
        if op.splitext(tmp_file)[1] == ".gz":
            gunzip(tmp_file, input_dir)
        elif op.splitext(tmp_file)[1] == ".zip":
            with ZipFile(tmp_file, "r") as zipObj:
                zipObj.extractall(input_dir)

        # Ensure that the gear is launching from /flywheel/v0/work, so the results
        # land where expected. (Internal, MRQy coding dictates cwd + UserInterface/Data)
        if os.getcwd() != work_dir:
            os.chdir(work_dir)

        result = sp.Popen(['python','/usr/local/bin/MRQy/src/mrqy/QC.py', result_name, input_dir],
                       stdout=sp.PIPE,
                       stderr=sp.PIPE,
                       universal_newlines=True
                       )
        stdout, stderr = result.communicate()
        if stdout and not stderr:
            log.info("QC'd %s", result_name)
            stdout_end = stdout.find('\n', 77)
            log.debug(stdout[77:stdout_end])

            # Handle the output as Sina mentioned by putting on .metadata.json or QC.json...
            csv_loc = op.join(work_dir,'UserInterface/Data', result_name)
            metadata_dict[i] = add_results_metadata(csv_loc, f.classification, f.name)

        elif stderr:
            log.error(stderr)

    retrieve_batch_results(work_dir, output_dir)
    gear_context.update_file_metadata('MRQy.zip', {'info': metadata_dict})
    return 0

