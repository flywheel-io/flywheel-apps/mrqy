"""Parser module to parse gear config.json."""
import gzip
import logging
import shutil
import os.path as op
from typing import Tuple

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_mrqy.hierarchy import get_matching_files

log = logging.getLogger(__name__)

# This function mainly parses gear_context's config.json file and returns relevant inputs and options.
def parse_config(
    gear_context: GearToolkitContext,
) -> Tuple[str, str]:
    """[Summary]

    Returns:
        [type]: [description]
    """

    debug = gear_context.config.get("debug")
    inputs = gather_mrqy_input_files(gear_context)
    log.debug(gear_context.get_input('file_to_analyze'))

    # if gear_context.get_input("file_to_analyze"):
    #     log.debug('I am trying to cherry pick an acquisition.')
    #     cherry_pick = gear_context.get_input_path("file_to_analyze")
    #     inputs = [i for i in inputs if i.name == cherry_pick]

    return debug, inputs


def gather_mrqy_input_files(gear_context):
    """
    Does this function change, depending on if it is at the project or session level?
    """
    # Determine the available DICOM acquisitions
    # Get the root_dir for the sessions, acquisitions, etc.
    destination_parent = gear_context.get_destination_parent()

    # Populate the possible search restrictions
    args_dict = {}
    args = ["filetype", "modality", "measurement"]
    for arg in args:
        if gear_context.config.get("restrict_to_"+arg):
            args_dict[arg] = gear_context.config.get("restrict_to_"+arg)
        else:
            args_dict[arg] = None

    log.info(f'Using restrictions for {[k for k,v in args_dict.items() if v]}')
    # Actually get the matches
    input_files = get_matching_files(
        destination_parent,
        filetype=args_dict["filetype"],
        modality=args_dict["modality"],
        measurement=args_dict["measurement"],
        context=gear_context,
    )

    if gear_context.config["debug"]:
        log.debug(f"input_file.file_id's: {[f.get('file_id') for f in input_files]}")

    # Return the list
    return input_files


def gunzip(filename, input_dir):
    """
    gunzip the file.
    Args:
        filename (path): zipped filename
    Returns:
        filename_out (path): unzipped filename
    """
    filename_out = op.join(input_dir, op.basename(op.splitext(filename)[0]))
    with gzip.open(filename, "rb") as f_in:
        with open(filename_out, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)
    return filename_out
